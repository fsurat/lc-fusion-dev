package lc.fusion.core.services;

import java.util.Dictionary;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.rewriter.Transformer;
import org.apache.sling.rewriter.TransformerFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.adobe.acs.commons.util.ParameterUtil;

@Component(service = TransformerFactory.class, immediate = true, property = { "label=Custom Image Link Transformer",
		Constants.SERVICE_DESCRIPTION + "=Image Link Transformer", "pipeline.type=customlinkrewriter" })

public class CustomImageLinkTransformer implements TransformerFactory {

	private Map<String, String[]> attributes;

	private static final String ATTR_CLASS = "class";

	private static final String CLASS_NOSTATIC = "nostatic";

	private String[] prefixes;

	private static final String PROP_ATTRIBUTES = "attributes";

	private static final String[] DEFAULT_ATTRIBUTES = new String[] { "img:src", "link:href", "script:src" };

	private static final String PROP_PREFIXES = "prefixes";

	private static final Logger LOG = LoggerFactory.getLogger(CustomImageLinkTransformer.class);

	public final class ImageLinkRewriteTransformer extends AbstractTransformer {

		public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
				throws SAXException {
			getContentHandler().startElement(namespaceURI, localName, qName,
					rebuildAttributes(localName, atts, request));
		}
	}

	public Transformer createTransformer() {
		return new ImageLinkRewriteTransformer();
	}

	private Attributes rebuildAttributes(final String elementName, final Attributes attrs,
			SlingHttpServletRequest request) {
		if (attributes.containsKey(elementName)) {
			final String[] modifyableAttributes = attributes.get(elementName);

			// clone the attributes
			final AttributesImpl newAttrs = new AttributesImpl(attrs);
			final int len = newAttrs.getLength();

			// first - check for the nostatic class
			boolean rewriteStatic = true;
			for (int i = 0; i < len; i++) {
				final String attrName = newAttrs.getLocalName(i);
				if (ATTR_CLASS.equals(attrName)) {
					String attrValue = newAttrs.getValue(i);
					if (attrValue.contains(CLASS_NOSTATIC)) {
						rewriteStatic = false;
					}
				}
			}

			if (rewriteStatic) {
				for (int i = 0; i < len; i++) {
					final String attrName = newAttrs.getLocalName(i);
					if (ArrayUtils.contains(modifyableAttributes, attrName)) {
						final String attrValue = newAttrs.getValue(i);
						for (String prefix : prefixes) {
							if (attrValue.startsWith(prefix)) {
								newAttrs.setValue(i, request.getResourceResolver().map(attrValue));
							}
						}
					}
				}
			}
			return newAttrs;
		} else {
			return attrs;
		}
	}

	@Activate
	protected void activate(final ComponentContext componentContext) {
		final Dictionary<?, ?> properties = componentContext.getProperties();

		final String[] attrProp = PropertiesUtil.toStringArray(properties.get(PROP_ATTRIBUTES), DEFAULT_ATTRIBUTES);
		this.attributes = ParameterUtil.toMap(attrProp, ":", ",");
		this.prefixes = PropertiesUtil.toStringArray(properties.get(PROP_PREFIXES), new String[0]);
	}
}