package lc.fusion.core.services;

import java.io.IOException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.rewriter.ProcessingComponentConfiguration;
import org.apache.sling.rewriter.ProcessingContext;
import org.apache.sling.rewriter.Transformer;
import org.osgi.annotation.versioning.ConsumerType;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 * Abstract base class to ease creating transformer pipeline components. All
 * methods are impelemented as pass-throughs to the next content handler.
 * Similar to Cocoon's AbstractSAXPipe.
 */
@ConsumerType
public abstract class AbstractTransformer implements Transformer {

	private ContentHandler contentHandler;
	SlingHttpServletRequest request;

	/**
	 * {@inheritDoc}Ø
	 */
	public void characters(final char[] ch, final int start, final int length) throws SAXException {
		contentHandler.characters(ch, start, length);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
	}

	/**
	 * {@inheritDoc}
	 */
	public void endDocument() throws SAXException {
		contentHandler.endDocument();
	}

	/**
	 * {@inheritDoc}
	 */
	public void endElement(final String uri, final String localName, final String qName) throws SAXException {
		contentHandler.endElement(uri, localName, qName);
	}

	/**
	 * {@inheritDoc}
	 */
	public void endPrefixMapping(final String prefix) throws SAXException {
		contentHandler.endPrefixMapping(prefix);
	}

	/**
	 * {@inheritDoc}
	 */
	public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
		contentHandler.ignorableWhitespace(ch, start, length);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(final ProcessingContext context, final ProcessingComponentConfiguration config)
			throws IOException {
		request = context.getRequest();
	}

	/**
	 * {@inheritDoc}
	 */
	public void processingInstruction(final String target, final String data) throws SAXException {
		contentHandler.processingInstruction(target, data);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void setContentHandler(final ContentHandler contentHandler) {
		this.contentHandler = contentHandler;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setDocumentLocator(final Locator locator) {
		contentHandler.setDocumentLocator(locator);
	}

	/**
	 * {@inheritDoc}
	 */
	public void skippedEntity(final String name) throws SAXException {
		contentHandler.skippedEntity(name);
	}

	/**
	 * {@inheritDoc}
	 */
	public void startDocument() throws SAXException {
		contentHandler.startDocument();
	}

	/**
	 * {@inheritDoc}
	 */
	public void startElement(final String uri, final String localName, final String qName, final Attributes atts)
			throws SAXException {
		contentHandler.startElement(uri, localName, qName, atts);
	}

	/**
	 * {@inheritDoc}
	 */
	public void startPrefixMapping(final String prefix, final String uri) throws SAXException {
		contentHandler.startPrefixMapping(prefix, uri);
	}

	protected final ContentHandler getContentHandler() {
		return contentHandler;
	}

}