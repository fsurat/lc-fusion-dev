'use strict';

use(function() {
	var brandTheme = properties.get("brandTheme", "");
	return {
		brand: brandTheme,
	};
});
