// // initiate the swiper plugin for carousel
// $(document).ready(function() {
//     var mySimpleSwiper = new Swiper('.simple-carousel', {
//         // Optional parameters
//         loop: true,
//         speed: 1500,
//         autoplay: 5000,
//         touchEventsTarget: 'carousel__slide',
//         autoHeight: true,
//         simulateTouch: true,
//         breakpoints: {
//             // when window width is <= 1200px
//             1200: {
//                 slidesPerView: 1,
//                 spaceBetween: 10,
//                 touchEventsTarget: 'container',
//                 noSwiping: false,
//             },
//             767: {
//                 spaceBetween: 10,
//                 slidesPerView: '1.3',
//                 centeredSlides: true,
//             }
//         },

//         // If we need pagination
//         pagination: '.swiper-pagination',

//         // Navigation arrows
//         nextButton: '.swiper-button-next',
//         prevButton: '.swiper-button-prev'

//     });

//     if ($(window).width() < 768) {
//         mySimpleSwiper.stopAutoplay();
//         mySimpleSwiper.enableTouchControl();
//     } else if ($(window).width() <= 768) {
//         mySimpleSwiper.enableTouchControl();
//     }
//     $(".simple-carousel").hover(function() {
//         mySimpleSwiper.stopAutoplay();
//     }, function() {
//         mySimpleSwiper.startAutoplay();
//     })
// });