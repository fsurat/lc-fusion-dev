/*
 * used to create Market Objects, Which holds market data.
 * has Getters and Setters to View or Modify market properties.
 * acts as Market Bean. 
 */
function Market(marketId, marketStatus, marketDisplayStatus, marketDisplayOrder, marketHref, evtObj){
	this.marketId = marketId;
	this.marketStatus = marketStatus;
	this.marketDisplayStatus = marketDisplayStatus;
	this.marketDisplayOrder = marketDisplayOrder;
	this.marketHref = marketHref;	
	this.evtObj = evtObj;
}

Market.prototype = {
	constructor : Market,
	
	getMarketId : function(){
		return this.marketId;
	},
	
	getMarketStatus : function(){
		return this.marketStatus;
	},
	
	getMarketDisplayStatus: function(){
		return this.marketDisplayStatus;
	},
	
	getMarketDisplayOrder:function(){
		return this.marketDisplayOrder;
	},	
	
	getMarketHref : function(){
		return this.marketHref;
	},
	
	getEventDetails: function(){
		return this.evtObj;
	},
	
	setMarketStatus : function(mstatus){
		this.marketStatus = mstatus;
	},

	setMarketDisplayStatus : function(mdstatus){
		this.marketDisplayStatus = mdstatus;
	},

	setMarketDisplayOrder : function(dorder){
		this.marketDisplayOrder = dorder;
	},
	
	getMarketInfo : function(){
		return "["+this.marketId+" "+this.marketStatus+" "+this.marketDisplayStatus+" "+this.marketDisplayOrder+" "+this.marketHref+" "+this.evtObj+"]";
	}
};