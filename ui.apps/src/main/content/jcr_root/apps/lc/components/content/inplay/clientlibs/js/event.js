/*
 * used to create Event Objects, Which holds event data.
 * has Getters and Setters to View or Modify event properties.
 * acts as Event Bean. 
 */
function Event(eventId, eventTitle, eventHref, eventStatus, eventDisplayStatus, eventDisplayOrder, eventDateTime, subTypeKey, subTypeName, subTypeDispOrder, subTypeHref, typeName, typeKey, typeDispOrder, classType){
	this.eventId = eventId;
	this.eventTitle = eventTitle;
	this.eventHref = eventHref;
	this.eventStatus = eventStatus;
	this.eventDisplayStatus = eventDisplayStatus;
	this.eventDisplayOrder = eventDisplayOrder;
	this.eventDateTime = eventDateTime;
	this.subTypeKey = subTypeKey;
	this.subTypeName = subTypeName;
	this.subTypeDispOrder = subTypeDispOrder;
	this.subTypeHref = subTypeHref;
	this.typeName = typeName;
	this.typeKey = typeKey;
	this.typeDispOrder = typeDispOrder;
	this.classType = classType;
}

Event.prototype = {
	constructor : Event,
	
	getEventId : function(){
		return this.eventId;
	},
	
	getEventTitle : function(){
		return this.eventTitle;
	},		

	getEventHref : function(){
		return this.eventHref;
	},
	
	getEventStatus : function(){
		return this.eventStatus;
	},
	
	getEventDisplayStatus: function(){
		return this.eventDisplayStatus;
	},
	
	getEventDisplayOrder:function(){
		return this.eventDisplayOrder;
	},
	
	getEventClassType : function(){
		return this.classType;
	},
	
	getEventDateTime : function(){
		return this.eventDateTime;
	},
	
	getSubTypeKey : function(){
		return this.subTypeKey;
	},

	getSubTypeName : function(){
		return this.subTypeName;
	},	
	
	getSubTypeDispOrder : function(){
		return this.subTypeDispOrder;
	},
	
	getTypeName : function(){
		return this.typeName;
	},

	getTypeKey : function(){
		return this.typeKey;
	},
	
	getTypeDispOrder : function(){
		return this.typeDispOrder;
	},
	
	setEventStatus : function(status){
		this.eventStatus = status;
	},
	
	setEventDisplayStatus : function(dispStatus){
		this.eventDisplayStatus = dispStatus;
	},
	
	setEventDisplayOrder : function(dispOrder){
		this.eventDisplayOrder = dispOrder;
	},
	
	setEventTitle : function(eventTitle){
		this.eventTitle = eventTitle;
	},
	
	setEventDateTime : function(eventDateTime){
		this.eventDateTime = eventDateTime;
	},
	
	
	getEventInfo : function(){
		return "["+this.eventId  +" "+	this.eventTitle +" "+this.eventHref +" "+this.eventStatus+" "+this.eventDisplayStatus+" "+this.eventDisplayOrder+" "+this.eventDateTime+" "+this.subTypeKey+" "+this.subTypeName+" "+this.subTypeDispOrder+" "+this.subTypeHref+" "+this.typeName+" "+this.typeKey+" "+this.typeDispOrder+" "+this.classType+"]";
	}
};