/******** Our main function ********/
function main(configObj) {

    $.support.cors = true;

    $.ajaxSetup({
        headers: { "Accept": "application/json" }
    });

    bip_main.init(configObj);
}

/******** The bet-in-play object which holds all other functions ********/
var bip_main = (function() {

    var self = this;

    self.obj = { "Y": {} }; //The pull data is gets stored in this object, used for rendering the DOM after pull complete's	
    self.eventArr = [];
    self.previousEventArr = [];
    self.eventArrForComp = [];
    self.previousEventArrForComp = [];
    self.diffEvents = [];
    self.eventArrToRender = [];
    self.logging = true; //flag to turnoff logging	
    self.firstTime = true;

    /******** The log function, wrapper to log the messages in the console ********/
    self.logMessage = function(message) {
        try {
            if (self.logging == true) {
                var time = new Date();
                var date = time.getFullYear() + '-' +
                    time.getMonth() + '-' +
                    time.getDate() + ' ' +
                    time.getHours() + ':' +
                    time.getMinutes() + ':' +
                    time.getSeconds() + ':' +
                    time.getMilliseconds();

                console.log('[' + date + '] ', message);
            }
        } catch (e) {}
    };

    /**This function creates a push connection, After a successful connection object
     * onPushConnection will be called, which is a callback function.
     */
    self.createPushConnection = function() {
        LadbrokesWebFactory.createConnection(configObj.webSocketUrl, configObj.apikey, null /*username*/ , null /*password*/ , self.onPushConnection);
    };

    /**This is a call back function which will be invoked, After a successful connection
     * and provides the connection object as an argument.
     */
    self.onPushConnection = function(connectionWrapperObj) {
        LadbrokesNotifications.doClassSubscription(connectionWrapperObj, configObj.topic, configObj.selector);
    };

    /***** The init function will be called only one time during the page load process ********/
    self.init = function(configObj) {

        self.logMessage("init triggered");

        //reload page every 45min
        setTimeout(function() {
            location.reload();
        }, 2700000);

        self.listEventsFromClassType(configObj);

        //check for new events every 45 sec.
        setInterval(function() {
            self.listEventsFromClassType(configObj);
        }, 45 * 1000);

    };

    /** This function makes ajax call's for class instance with expand filter upto Event,
     *  ie., Football and Tennis class types data will be returned. 
     */
    self.listEventsFromClassType = function(configObj) {
        var classCollectionUrlArray = configObj.getClassCollectionUrlArray();

        //storing previous events, will be using during comparision.
        self.previousEventArrForComp = self.eventArrForComp;
        self.previousEventArr = self.eventArr;

        //reset arrays
        self.eventArrForComp = [];
        self.eventArr = [];
        self.eventArrToRender = [];

        // set up a deffered promise to fire when all our async calls come back
        var defers = [],
            defer;
        var urlsLength = classCollectionUrlArray.length;

        for (var i = 0, j = urlsLength; i < j; i++) {

            var classInstanceHref = classCollectionUrlArray[i].CLASSTYPE.CLASS_URL;
            var eventInstanceHref = classCollectionUrlArray[i].CLASSTYPE.EVENT_URL;

            (function(classInstanceHref, eventInstanceHref) {
                self.logMessage("\n classInstanceHrefurl=" + classInstanceHref + " \n eventInstanceHrefurl=" + eventInstanceHref);

                defer = $.ajax({
                    type: 'GET',
                    url: classInstanceHref,
                    success: function(json) {
                        self.pullEventsData(json, eventInstanceHref);
                        self.logMessage(JSON.stringify(self.eventArr));
                        self.logMessage("\n number of events received after a successful pull:" + self.eventArr.length);

                    },
                    error: function(err) {
                        self.logMessage("classUrl communication error: " + err);
                    }
                });

            })(classInstanceHref, eventInstanceHref);

            defers.push(defer);
        }

        $.when.apply(window, defers).then(function() {
            // Do Something now that we have all the Class type data.
            self.logMessage("Done fetching all class type's data");
            if (self.firstTime == true) {
                //self.eventArr.shift();
                //self.eventArrForComp.shift();
                self.pullSelectionsData(self.eventArr, self.onAllPullComplete);
            } else {
                //self.eventArr.shift();
                //self.eventArrForComp.shift();
                self.compareEvents(self.eventArrForComp, self.previousEventArrForComp);
            }
        });

    };

    /** This function is used to find the Delta set of events when compared to the previous pull.**/
    self.compareEvents = function(eventArr, previousEventArr) {
        self.logMessage("eventArr: " + JSON.stringify(eventArr) + " :: " + "previousEventArr: " + JSON.stringify(previousEventArr));
        self.diffEvents = $.merge($(eventArr).not(previousEventArr).get(), $(previousEventArr).not(eventArr).get());
        self.logMessage("self.diffEvents: " + JSON.stringify(self.diffEvents));
        if (self.diffEvents.length > 0) {
            renderDifferenceEvents(self.diffEvents);
        }
    };

    /** This function renders the Delta set of events which we got after comparision with previous pull.**/
    self.renderDifferenceEvents = function(diffEvents) {
        diffEvents = $.grep(diffEvents, function(_event, index) {
            var _eventId = _event.split("_")[0];
            self.logMessage("eventId diff: " + _eventId);
            if ($("#event_" + _eventId).length > 0) {
                // logic to remove resulted events from DOM after comparision.
                self.logMessage("eventId to remove: " + _eventId);
                if ($("#event_" + _eventId).parent().find("tr[id*='event_']").size() > 1) {
                    $("#event_" + _eventId).prev().remove();
                    $("#event_" + _eventId).remove();
                    return false;
                } else {
                    $("#event_" + _eventId).closest("div[class^='content']").prev().remove();
                    $("#event_" + _eventId).closest("div[class^='content']").remove();
                    return false;
                }

            } else {
                // logic to add new in-play events to DOM after comparision.
                $.each(self.eventArr, function(index, _event) {
                    if (_eventId == _event["event_id"]) {
                        self.eventArrToRender.push(_event);
                    }
                });
            }

            return true;
        });
        self.diffEvents = diffEvents;

        if (self.eventArrToRender.length > 0) {
            // logic to add events in DOM after comparision.
            self.logMessage("self.eventArrToRender: " + JSON.stringify(self.eventArrToRender));
            self.pullSelectionsData(self.eventArrToRender, self.onAllPullComplete);
        } else {
            self.logMessage("no new or resulted events to render ");
        }
    };

    /** Constructs an Array of Events and Events Url with the json data, 
     *  that we got after class instance pull operation, this contains the possible conditions to find an event element.
     *  examples like Type is an Array/Object, SubType is an Array/Object, SubType is not present as part of Hierarchy.
     *   . 
     */
    self.pullEventsData = function(data, eventInstanceHref) {
        (function(data, eventInstanceHref) {
            if (!jQuery.isEmptyObject(data.classes.class)) {
                var classType = data.classes.class.classKey;
                self.obj["Y"][classType] = { "name": data.classes.class.className, "disporder": data.classes.class.displayOrder }
                if (jQuery.isArray(data.classes.class.types.type)) { // If type is an Array.
                    var typeArray = data.classes.class.types.type;
                    $.each(typeArray, function(index, type) {
                        var subTypeName = "";
                        var subTypeKey = "";
                        var subTypeDispOrder = "";
                        var typeName = type.typeName;
                        var typeKey = type.typeKey;
                        var typeDispOrder = type.displayOrder;
                        if (type.subtypes) { //if subType is present.
                            // If type and subType are array's .
                            if (type.subtypes.subtype && !jQuery.isEmptyObject(type.subtypes.subtype) && jQuery.isArray(type.subtypes.subtype)) {
                                var subTypeArr = type.subtypes.subtype;
                                $.each(subTypeArr, function(index, subTypeObj) {
                                    subTypeName = subTypeObj.subTypeName;
                                    subTypeKey = subTypeObj.subTypeKey;
                                    subTypeDispOrder = subTypeObj.displayOrder;
                                    // If type, subType, events are array's .
                                    if (subTypeObj.events.event && !jQuery.isEmptyObject(subTypeObj.events.event) && jQuery.isArray(subTypeObj.events.event)) {
                                        var eventArray = subTypeObj.events.event;
                                        $.each(eventArray, function(index, event) {
                                            self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                                        });

                                    } // If type, subType are array's and event is an object.
                                    else if (subTypeObj.events.event && !jQuery.isEmptyObject(subTypeObj.events.event)) {
                                        var event = subTypeObj.events.event;
                                        self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                                    }
                                });
                            }
                            // If type is an Array and subType is an Object and events is an Array.
                            else if (type.subtypes.subtype && !jQuery.isEmptyObject(type.subtypes.subtype) && jQuery.isArray(type.subtypes.subtype.events.event)) {
                                subTypeName = type.subtypes.subtype.subTypeName;
                                subTypeKey = type.subtypes.subtype.subTypeKey;
                                subTypeDispOrder = type.subtypes.subtype.displayOrder;

                                var eventArray = type.subtypes.subtype.events.event;
                                $.each(eventArray, function(index, event) {
                                    self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                                });
                            }
                            // If type is an Array and subType is an Object and event is an Object.
                            else if (!jQuery.isEmptyObject(type.subtypes.subtype.events)) {
                                subTypeName = type.subtypes.subtype.subTypeName;
                                subTypeKey = type.subtypes.subtype.subTypeKey;
                                subTypeDispOrder = type.subtypes.subtype.displayOrder;
                                var event = type.subtypes.subtype.events.event;
                                self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                            }

                        } else { // If subType is not present and event is an Array, happens for Tennis..						  
                            if (!jQuery.isEmptyObject(type.events) && jQuery.isArray(type.events.event)) {
                                var eventArray = type.events.event;
                                $.each(eventArray, function(index, event) {
                                    self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                                });
                            } // If subType is not present and event is an Object, happens for Tennis.
                            else if (!jQuery.isEmptyObject(type.events)) {
                                var event = type.events.event;
                                self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                            }

                        }
                    });
                }
                // If type is an Object.
                else if (!jQuery.isEmptyObject(data.classes.class.types.type)) {
                    var subTypeName = "";
                    var subTypeKey = "";
                    var subTypeDispOrder = "";
                    var type = data.classes.class.types.type;
                    var typeName = type.typeName;
                    var typeKey = type.typeKey;
                    var typeDispOrder = type.displayOrder;
                    if (type.subtypes) {
                        //If type is an object and subType is an Array.
                        if (type.subtypes.subtype && !jQuery.isEmptyObject(type.subtypes.subtype) && jQuery.isArray(type.subtypes.subtype)) {
                            var subTypeArr = type.subtypes.subtype;
                            $.each(subTypeArr, function(index, subTypeObj) {
                                subTypeName = subTypeObj.subTypeName;
                                subTypeKey = subTypeObj.subTypeKey;
                                subTypeDispOrder = subTypeObj.displayOrder;
                                //If type is an object and both subType and events are Array.
                                if (subTypeObj.events.event && !jQuery.isEmptyObject(subTypeObj.events.event) && jQuery.isArray(subTypeObj.events.event)) {
                                    var eventArray = subTypeObj.events.event;
                                    $.each(eventArray, function(index, event) {
                                        self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                                    });

                                } //If type is an object and subType is an Array and event is an Object.
                                else if (subTypeObj.events.event && !jQuery.isEmptyObject(subTypeObj.events.event)) {
                                    var event = subTypeObj.events.event;
                                    self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                                }
                            });

                        } //If type is an object and subType is an Object and event is an Array.
                        else if (type.subtypes.subtype && !jQuery.isEmptyObject(type.subtypes.subtype) && jQuery.isArray(type.subtypes.subtype.events.event)) {
                            subTypeName = type.subtypes.subtype.subTypeName;
                            subTypeKey = type.subtypes.subtype.subTypeKey;
                            subTypeDispOrder = type.subtypes.subtype.displayOrder;
                            var eventArray = type.subtypes.subtype.events.event;
                            $.each(eventArray, function(index, event) {
                                self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                            });
                        } //If type is an object and subType is an Object and event is an Object.
                        else if (!jQuery.isEmptyObject(type.subtypes.subtype.events)) {
                            subTypeName = type.subtypes.subtype.subTypeName;
                            subTypeKey = type.subtypes.subtype.subTypeKey;
                            subTypeDispOrder = type.subtypes.subtype.displayOrder;
                            var event = type.subtypes.subtype.events.event;
                            self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                        }
                    } //If type is an object and subType is not present, happens for Tennis.
                    else {
                        //If type is an object and subType is not present and event is an Array.
                        if (!jQuery.isEmptyObject(type.events) && jQuery.isArray(type.events.event)) {
                            var eventArray = type.events.event;
                            $.each(eventArray, function(index, event) { //nosubtype for tennis
                                self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                            });
                        } //If type is an object and subType is not present and event is an Object.
                        else if (!jQuery.isEmptyObject(type.events)) {
                            var event = type.events.event;
                            self.buildEventBean(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref);
                        }
                    }
                }
            }
        })
        (data, eventInstanceHref);
    };

    /** Constructs an Event Bean, which holds data of type, subType, eventUrl, classType and Display Order. 
     *  This bean get's pushed into an Event Array, which will be used to make individual event pull.  
     */
    self.buildEventBean = function(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType, eventInstanceHref) {
        (function(event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType) {
            var eventKey = event.eventKey;
            var eventTitle = event.eventName;
            var eventUrl = eventInstanceHref.replace("EVENT_KEY", eventKey);
            var eventStatus = event.eventStatus;
            var eventDisplayStatus = "";
            var eventDisplayOrder = "";
            var eventDateTime = "";
            var eventObj = new Event(eventKey, eventTitle, eventUrl, eventStatus, eventDisplayStatus, eventDisplayOrder, eventDateTime, subTypeKey, subTypeName, subTypeDispOrder, "", typeName, typeKey, typeDispOrder, classType);
            self.logMessage("\n eventObj ==" + eventObj.getEventInfo());
            var obj = new Object();
            obj["event_id"] = eventKey;
            obj["event_data"] = eventObj;
            obj["event_href"] = eventUrl;
            self.eventArr.push(obj);
            self.eventArrForComp.push(eventKey + "_" + eventUrl);
        })
        (event, subTypeKey, subTypeName, subTypeDispOrder, typeName, typeKey, typeDispOrder, classType);
    };

    /** Process Event Data that we got as part of Ajax response and construct a global Obj,
     *  which holds all data related to all events and class types.  
     */
    self.processEventData = function(data, eventObj) {
        eventObj.setEventDisplayStatus(data.event.displayStatus);
        eventObj.setEventDisplayOrder(data.event.displayOrder);
        eventObj.setEventTitle(data.event.eventName);
        eventObj.setEventDateTime(getCurrentUserTime(data.event.eventDateTime));
        eventObj.setEventStatus(data.event.eventStatus);
        self.logMessage("\n latest eventObj ==" + eventObj.getEventInfo());
        //If market is an Array, Look for Win-Draw-Win or Match betting markets only.
        if (!jQuery.isEmptyObject(data.event.markets) && jQuery.isArray(data.event.markets.market)) {
            var marketArray = data.event.markets.market;
            $.each(marketArray, function(index, market) {
                self.buildEventsWithMarket(eventObj, market);
            });
        } //If market is an Object, Look for Win-Draw-Win or Match betting markets only.
        else if (!jQuery.isEmptyObject(data.event.markets)) {
            var market = data.event.markets.market;
            self.buildEventsWithMarket(eventObj, market);
        }
    };

    /** Construct global Obj with event, market and selection outcomes.
     * also sorts the events and thier outcomes based on displayorder attribute.  
     */
    self.buildEventsWithMarket = function(eventObj, market) {
        var classType = eventObj.getEventClassType();
        var selectionArr = null;
        if (market.marketName == "Win-Draw-Win" || market.marketName == "Match Betting" || market.marketName == "Match betting" || market.marketName == "|Win-Draw-Win|") {
            if (market.selections) {
                selectionArr = market.selections.selection;
                var marketObj = new Market(market.marketKey, market.marketStatus, market.displayStatus, market.displayOrder, "", eventObj);

                if (eventObj.getEventClassType() == '110000006') { // Football ClassType.
                    if (self.obj["Y"][classType][eventObj.getSubTypeKey()]) {
                        // do nothing, if subType id is present in the global object.
                    } else { // construct a new subType If the global object is not having the subType id.
                        self.obj["Y"][classType][eventObj.getSubTypeKey()] = { "name": eventObj.getSubTypeName(), "disporder": eventObj.getSubTypeDispOrder(), "typedisporder": eventObj.getTypeDispOrder(), events: [] };
                    }
                    var eventJsonObj = {};
                    eventJsonObj["marketStatus"] = market.marketStatus;
                    eventJsonObj["marketDispStatus"] = market.displayStatus;
                    eventJsonObj["market_id"] = market.marketKey;
                    eventJsonObj["marketDispOrder"] = market.displayOrder;
                    selectionArr.sort(function(a, b) { return a.displayOrder - b.displayOrder });
                    eventJsonObj["outcomes"] = selectionArr;
                    eventJsonObj["eventStatus"] = eventObj.getEventStatus();
                    eventJsonObj["eventid"] = eventObj.getEventId();
                    eventJsonObj["name"] = eventObj.getEventTitle();
                    eventJsonObj["dispStatus"] = eventObj.getEventDisplayStatus();
                    eventJsonObj["dispOrder"] = eventObj.getEventDisplayOrder();
                    eventJsonObj["time"] = eventObj.getEventDateTime();

                    self.obj["Y"][classType][eventObj.getSubTypeKey()].events.push(eventJsonObj);
                    //sort the events based on display order.
                    self.obj["Y"][classType][eventObj.getSubTypeKey()].events.sort(function(a, b) { return b.dispOrder - a.dispOrder });

                    return;
                } else if (eventObj.getEventClassType() == '110000009') { // Tennis Class Type.
                    if (self.obj["Y"][classType][eventObj.getTypeKey()]) {
                        // do nothing, if subType id is present in the global object.
                    } else { // construct a new subType If the global object is not having the subType id.
                        self.obj["Y"][classType][eventObj.getTypeKey()] = { "name": eventObj.getTypeName(), "disporder": eventObj.getTypeDispOrder(), events: [] };
                    }

                    var eventJsonObj = {};
                    eventJsonObj["marketStatus"] = market.marketStatus;
                    eventJsonObj["marketDispStatus"] = market.displayStatus;
                    eventJsonObj["market_id"] = market.marketKey;
                    eventJsonObj["marketDispOrder"] = market.displayOrder;
                    selectionArr.sort(function(a, b) { return a.displayOrder - b.displayOrder });
                    eventJsonObj["outcomes"] = selectionArr;
                    eventJsonObj["eventStatus"] = eventObj.getEventStatus();
                    eventJsonObj["eventid"] = eventObj.getEventId();
                    eventJsonObj["name"] = eventObj.getEventTitle();
                    eventJsonObj["dispStatus"] = eventObj.getEventDisplayStatus();
                    eventJsonObj["dispOrder"] = eventObj.getEventDisplayOrder();
                    eventJsonObj["time"] = eventObj.getEventDateTime();

                    self.obj["Y"][classType][eventObj.getTypeKey()].events.push(eventJsonObj);
                    //sort the events based on display order.
                    self.obj["Y"][classType][eventObj.getTypeKey()].events.sort(function(a, b) { return b.dispOrder - a.dispOrder });

                    return;
                }
            }
        }
    };

    /** This function makes multiple ajax call's for each eventUrl present in the event collection
     *  deffered promise to fire when all our event async calls come back. 
     */
    self.pullSelectionsData = function(eventArr, onAllPullComplete) {

        // set up a deffered promise to fire when all our async calls come back
        var defers = [],
            defer;
        var urlsLength = eventArr.length;

        for (var i = 0, j = urlsLength; i < j; i++) {
            var eventUrl = eventArr[i].event_href;
            var eventObj = eventArr[i].event_data;
            (function(eventUrl, eventObj) {
                self.logMessage("\n eventHref =" + eventObj.getEventHref());
                defer = $.ajax({
                    type: 'GET',
                    url: eventUrl,
                    success: function(json) {
                        self.processEventData(json, eventObj);
                    },
                    error: function(err) {
                        self.logMessage("eventUrl communication error: " + err);
                    }
                });

            })(eventUrl, eventObj);

            defers.push(defer);
        }

        $.when.apply(window, defers).then(function() {
            // Do Something now that we have all the data	     
            onAllPullComplete();
        });
    };

    /** This function gets fired when all the pull operations on a page are complete.
     *  renders the final Object which contains data related to all events of all class types. 
     */
    self.onAllPullComplete = function() {
        self.logMessage("\n Done fetching all events data, render the page now " + JSON.stringify(self.obj));
        if (self.obj["Y"]["110000006"])
            self.renderFootBallData(self.obj["Y"]["110000006"]);
        if (self.obj["Y"]["110000009"])
            self.renderTennisData(self.obj["Y"]["110000009"]);

        if (self.firstTime == true) { // execute only when the pageloads.	  
            $(".se-pre-con").fadeOut("slow");
            self.createPushConnection();
            self.firstTime = false;
        }
    };

    /** render football data, on to the football place holder in the webpage. 
     */
    self.renderFootBallData = function(footballData) {
        var sortable = [];
        for (var obj in footballData) {
            sortable.push([obj, footballData[obj]])
        }
        // sort the subtypes based on type's display order.
        sortable.sort(function(a, b) { return a[1].typedisporder - b[1].typedisporder })
        $.each(sortable, function(index, obj) {
            var subTypeKey = obj[0];
            var subTypeObj = obj[1];
            if (subTypeKey == "name" || subTypeKey == "disporder") {
                return;
            }
            var subTypeKey = subTypeKey;
            var subTypeName = subTypeObj.name;
            if (subTypeObj.events instanceof Array) {
                $.each(subTypeObj.events, function(index, event) {
                    var eventKey = event.eventid;
                    var eventTitle = event.name;
                    var eventDateTime = event.time;
                    var marketKey = event.market_id;
                    var eventStatus = event.eventStatus;
                    var marketStatus = event.marketStatus;
                    var selectionArr = event.outcomes;
                    var greyOut = "";
                    if (eventStatus === "Suspended" || marketStatus === "Suspended") {
                        greyOut = "class=\"grey-out\"";
                    }
                    var homeSelectionObj = {};
                    var drawSelectionObj = {};
                    var awaySelectionObj = {};
                    if (selectionArr instanceof Array) {
                        for (var i in selectionArr) {
                            var selectionObj = selectionArr[i];
                            if (selectionObj.outcomeMeaningMinorCode === "H") {
                                homeSelectionObj.id = selectionObj.selectionKey;
                                homeSelectionObj.title = selectionObj.selectionName;
                                homeSelectionObj.price = selectionObj.currentPrice.numPrice + "/" + selectionObj.currentPrice.denPrice;
                                homeSelectionObj.selectionStatus = selectionObj.selectionStatus;
                            } else if (selectionObj.outcomeMeaningMinorCode === "D") {
                                drawSelectionObj.id = selectionObj.selectionKey;
                                drawSelectionObj.title = selectionObj.selectionName;
                                drawSelectionObj.price = selectionObj.currentPrice.numPrice + "/" + selectionObj.currentPrice.denPrice;
                                drawSelectionObj.selectionStatus = selectionObj.selectionStatus;
                            } else if (selectionObj.outcomeMeaningMinorCode === "A") {
                                awaySelectionObj.id = selectionObj.selectionKey;
                                awaySelectionObj.title = selectionObj.selectionName;
                                awaySelectionObj.price = selectionObj.currentPrice.numPrice + "/" + selectionObj.currentPrice.denPrice;
                                awaySelectionObj.selectionStatus = selectionObj.selectionStatus;
                            }
                        }
                    }

                    //var footballmatches = eventTitle.match(/^(.+)\s(\d*)(?:-| v | vs )+(\d*)\s(.+)$/i);
                    var eventTitle1 = homeSelectionObj.title;
                    var eventTitle2 = awaySelectionObj.title;

                    /*if (footballmatches && footballmatches.length >= 5) {
                    	eventTitle1 = footballmatches[1];
                    	eventTitle2 = footballmatches[4];
                    }*/

                    var homeSelGreyOut = "class=\"betbox-sel\"";
                    var drawSelGreyOut = "class=\"betbox-sel\"";
                    var awaySelGreyOut = "class=\"betbox-sel\"";
                    if (homeSelectionObj.selectionStatus === "Suspended") {
                        homeSelGreyOut = "class=\"betbox-sel grey-out\"";
                        homeSelectionObj.price = 'SUSP';
                        homeSelectionObj.id = 'SUSP';
                    }
                    if (drawSelectionObj.selectionStatus === "Suspended") {
                        drawSelGreyOut = "class=\"betbox-sel grey-out\"";
                        drawSelectionObj.price = 'SUSP';
                        drawSelectionObj.id = 'SUSP';
                    }
                    if (awaySelectionObj.selectionStatus === "Suspended") {
                        awaySelGreyOut = "class=\"betbox-sel grey-out\"";
                        awaySelectionObj.price = 'SUSP';
                        awaySelectionObj.id = 'SUSP';
                    }
                    // var arrowIconImage = 'red-icon.png';
                    // var coralBrand = $('.main-div')hasClass('coral');
                    // if (coralBrand) {
                    // 	arrowIconImage = 'green-icon.png';
                    // }
                    var subHeadingDiv = "<div  onClick=\"toogleClick(this); \" class=\"subTitle heading\" id=\"" + subTypeKey + "\">" + "<h3>" + subTypeName + "</h3><span class=\"minus-img\"></span>" +
                        "</div>" + "<div class=\"content clear\">" + "<table class=\"tblGrid\">" +
                        "<tr id=\"" + subTypeKey + "_rowData" + "\">"


                    var tableHead = "<th>&nbsp;  </th><th> TIME </th><th> EVENT </th><th class=\"th-space\"> H </th><th class=\"th-space\"> D </th><th class=\"th-space\"> A </th><th class=\"th-space\">FAVS</th></tr>";

                    var messageRowDiv = "<tr id=\"market_" + marketKey + "\"><td colspan=" + "\"7\"" + "><div class=\"div-note-disp\" ></div></td></tr>";

                    var eventTitleRowDiv = messageRowDiv + "<tr " + greyOut + " id=\"event_" + eventKey + "\"><td class=\"td-w5p arrow-icon\"><img class=\"arrow-icon\" src=\"/etc/designs/lc/components/inplay/red-icon.png\" alt=\"arrow icon\"/></td>" +
                        "<td class=\"td-w10p\">" + eventDateTime + "</td><td class=\"td-w40p\" >" +
                        "<div class=\"event-name1\" >" + eventTitle1 + "</div> <span class=\"left_align\"> v </span><div class=\"event-name2\">" + eventTitle2 + "</div></td>" +
                        "<td class=\"td-w15p\" id=\"selection_" + homeSelectionObj.id + "\"><div " + homeSelGreyOut + " data-eventid=" + eventKey + " data-marketid=" + marketKey + " onclick=\"selectionClickHandler(this);\">" + "<span id=\"" + homeSelectionObj.id + "\">" + homeSelectionObj.price + "</span>" + "</div></td>" +
                        "<td class=\"td-w15p\" id=\"selection_" + drawSelectionObj.id + "\"><div " + drawSelGreyOut + " data-eventid=" + eventKey + " data-marketid=" + marketKey + " onclick=\"selectionClickHandler(this);\">" + "<span id=\"" + drawSelectionObj.id + "\">" + drawSelectionObj.price + "</span>" + "</div></td>" +
                        "<td class=\"td-w15p\" id=\"selection_" + awaySelectionObj.id + "\"><div " + awaySelGreyOut + " data-eventid=" + eventKey + " data-marketid=" + marketKey + " onclick=\"selectionClickHandler(this);\">" + "<span id=\"" + awaySelectionObj.id + "\">" + awaySelectionObj.price + "</span>" + "</div></td>" +
                        "<td class=\"td-w15p\"><div>" + "<input class=\"checkbox football original\" type='checkbox' id=\"" + eventKey + "\"/><label for=\"" + eventKey + "\"></label>" + "</div></td>"
                    "</tr>";

                    if ($('.main-div').hasClass('coral')) {
                        $('.arrow-icon').attr("src", "/etc/designs/lc/components/inplay/green-icon.png");
                    }

                    // Mobile structures
                    var tableHeadMobile = "<th> TIME </th><th> EVENT </th><th colspan=\"2\"> ODDS </th></tr>";

                    var eventTitleRowDivMobile = messageRowDiv + "<tr " + greyOut + " id=\"event_" + eventKey + "\">" +
                        "<td rowspan=\"3\" class=\"td-w15p\">" + eventDateTime + "</td><td rowspan=\"3\" class=\"td-w40p\">" +
                        "<div class=\"event-name1\" >" + eventTitle1 + "</div> <span class=\"left_align\"> v </span><div class=\"event-name2\">" + eventTitle2 + "</div></td>" +
                        "<td>HOME</td><td class=\"td-w40p\" id=\"selection_" + homeSelectionObj.id + "\"><div " + homeSelGreyOut + " data-eventid=" + eventKey + " data-marketid=" + marketKey + " onclick=\"selectionClickHandler(this);\">" + "<span id=\"" + homeSelectionObj.id + "\">" + homeSelectionObj.price + "</span>" + "</div></td>" + "</tr><tr>" +
                        "<td>DRAW</td><td class=\"td-w40p\" id=\"selection_" + drawSelectionObj.id + "\"><div " + drawSelGreyOut + " data-eventid=" + eventKey + " data-marketid=" + marketKey + " onclick=\"selectionClickHandler(this);\">" + "<span id=\"" + drawSelectionObj.id + "\">" + drawSelectionObj.price + "</span>" + "</div></td>" + "</tr><tr>" +
                        "<td>AWAY</td><td class=\"td-w40p\" id=\"selection_" + awaySelectionObj.id + "\"><div " + awaySelGreyOut + " data-eventid=" + eventKey + " data-marketid=" + marketKey + " onclick=\"selectionClickHandler(this);\">" + "<span id=\"" + awaySelectionObj.id + "\">" + awaySelectionObj.price + "</span>" + "</div></td>" +
                        "</tr>";
                    // /Mobile structures

                    var tableEndDiv = "</table></div>";

                    var screenWidth = $(window).width();

                    if ($("#" + subTypeKey + "_rowData").length > 0 && screenWidth > 480) {
                        $("#" + subTypeKey + "_rowData").after(eventTitleRowDiv);
                    } else if ($("#" + subTypeKey + "_rowData").length > 0 && screenWidth <= 480) {
                        $("#" + subTypeKey + "_rowData").after(eventTitleRowDivMobile);
                    } else if (screenWidth <= 480) {
                        $("#footBall").append(subHeadingDiv + tableHeadMobile + eventTitleRowDivMobile + tableEndDiv);
                    } else {
                        $("#footBall").append(subHeadingDiv + tableHead + eventTitleRowDiv + tableEndDiv);
                    }
                });

                //favourites toggling

                $('.checkbox.football.original').off().click(function() {
                    var checkBox = this.id;
                    var checkBoxClone = this.id + 'fav'
                    var eventBox = "event_" + this.id;
                    var eventBoxClone = eventBox + 'fav';
                    var ticked = this.checked;
                    if (ticked) {
                        $('#' + eventBox).clone(true, true).off().appendTo("#myBetsFavourites .football .contentInner").attr('id', eventBoxClone).insertAfter("#id"); //grabs the favourited row, copies it, appends it to favourites row, and add 'fav' to the id

                        function fixIds(elem, tag) {
                            $(elem).find("[id]").each(function() {
                                this.id = this.id + tag;
                            })
                        }

                        function fixCheckBoxClass(elem, tag) {
                            $(elem).removeClass('original').addClass(tag); //swap the checkbox's original class to fav
                        }
                        $('#' + eventBoxClone).off();
                        fixIds('#' + eventBoxClone, 'fav');
                        fixCheckBoxClass('#' + checkBoxClone, 'favEvent')
                    } else {
                        $('#' + eventBoxClone).remove();
                    }
                });
            }
        });
    };
    /** render tennis data, on to the tennis place holder in the webpage.
     */
    self.renderTennisData = function(tennisData) {
        var sortable = [];
        for (var obj in tennisData) {
            sortable.push([obj, tennisData[obj]])
        }
        // sort the types based on type's display order.
        sortable.sort(function(a, b) { return a[1].disporder - b[1].disporder })
        $.each(sortable, function(index, obj) {
            var typeKey = obj[0];
            var typeObj = obj[1];
            if (typeKey == "name" || typeKey == "disporder") {
                return;
            }
            var typeName = typeObj.name;
            if (typeObj.events instanceof Array) {
                $.each(typeObj.events, function(index, event) {
                    var eventKey = event.eventid;
                    var eventTitle = event.name;
                    var eventDateTime = event.time;
                    var marketKey = event.market_id;
                    var eventStatus = event.eventStatus;
                    var marketStatus = event.marketStatus;
                    var selectionArr = event.outcomes;
                    var greyOut = "";
                    if (eventStatus === "Suspended" || marketStatus === "Suspended") {
                        greyOut = "class=\"grey-out\"";
                    }
                    var titleDiv = "";
                    var priceInfoDiv = "";
                    var currentSelectionObj = {};
                    if (selectionArr instanceof Array) {
                        for (var i in selectionArr) {
                            var selectionObj = selectionArr[i];
                            currentSelectionObj.id = selectionObj.selectionKey;
                            currentSelectionObj.title = selectionObj.selectionName;
                            currentSelectionObj.price = selectionObj.currentPrice.numPrice + "/" + selectionObj.currentPrice.denPrice;
                            titleDiv += "<div>" + currentSelectionObj.title + "</div>";
                            priceInfoDiv += "<div id=\"selection_" + currentSelectionObj.id + "\"" + ">" + "<div" + " data-eventid=" + eventKey + " data-marketid=" + marketKey + " class=\"betbox-sel\" onclick=\"selectionClickHandler(this);\">" + "<span id=\"" + currentSelectionObj.id + "\">" + currentSelectionObj.price + "</span>" + "</div>" + "</div>";
                        }
                    }
                    var subHeadingDiv = "<div class=\"content\"><div onClick=\"toogleClick(this); \" class=\"subTitle heading\" id=\"" + typeKey + "\">" +
                        "<h3>" + typeName + "</h3><span class=\"minus-img\"></span>" + "</div>" + "<div class=\"content \">" + "<table class=\"tblGrid\">" +
                        "<tr id=\"" + typeKey + "_rowData" + "\"><th>&nbsp;  </th><th> TIME </th><th> EVENT </th><th class=\"th-space\"> ODDS </th><th class=\"th-space\"> FAVS </th></tr>";
                    var messageRowDiv = "<tr id=\"market_" + marketKey + "\"><td colspan=" + "\"7\"" + "><div class=\"div-note-disp\"></div></td></tr>";
                    var eventTitleRowDiv = messageRowDiv + "<tr " + greyOut + " id=\"event_" + eventKey + "\"><td class=\"td-w5p\"><img class=\"arrow-icon\" src=\"/etc/designs/lc/components/inplay/red-icon.png\" alt=\"red icon\"/></td>" +
                        "<td class=\"td-w10p\">" + eventDateTime + "</td><td class=\"td-w40p\">" + titleDiv + "</td>" + "<td style=\"width:45%;\">" + priceInfoDiv + "</td>" +
                        "<td class=\"td-w15p\"><div>" + "<input class=\"checkbox tennis original\" type='checkbox' id=\"" + eventKey + "\"/><label for=\"" + eventKey + "\"></label>" + "</div></tr>";
                    var tableEndDiv = "</table></div></div>";

                    if ($('.main-div').hasClass('coral')) {
                        $('.arrow-icon').attr("src", "/etc/designs/lc/components/inplay/green-icon.png");
                    }

                    if ($("#" + typeKey + "_rowData").length > 0) {
                        $("#" + typeKey + "_rowData").after(eventTitleRowDiv);
                    } else {
                        $("#tennis").append(subHeadingDiv + eventTitleRowDiv + tableEndDiv);
                    }

                    if ($('.main-div').hasClass('coral')) {
                        $('.arrow-icon').attr("src", "/etc/designs/lc/components/inplay/green-icon.png");
                    }

                });

                //favourites toggling

                $('.checkbox.tennis.original').off().click(function() {
                    var checkBox = this.id;
                    var checkBoxClone = this.id + 'fav'
                    var eventBox = "event_" + this.id;
                    var eventBoxClone = eventBox + 'fav';
                    var ticked = this.checked;
                    if (ticked) {
                        $('#' + eventBox).clone(true, true).off().appendTo("#myBetsFavourites .tennis .contentInner").attr('id', eventBoxClone).insertAfter("#id"); //grabs the favourited row, copies it, appends it to favourites row, and add 'fav' to the id

                        function fixIds(elem, tag) {
                            $(elem).find("[id]").each(function() {
                                this.id = this.id + tag;
                            })
                        }

                        function fixCheckBoxClass(elem, tag) {
                            $(elem).removeClass('original').addClass(tag); //swap the checkbox's original class to fav
                        }
                        $('#' + eventBoxClone).off();
                        fixIds('#' + eventBoxClone, 'fav');
                        fixCheckBoxClass('#' + checkBoxClone, 'favEvent')
                    } else {
                        $('#' + eventBoxClone).remove();
                    }
                });
            }

        });
    };

    self.selectionArr = [];

    /*
     * this method gets notified whenever a selection is selected. This also doesn't allow suspended or not displayed
     * selections to be added to betnow and also removes the selections which are suspended from betnow.
     *
     */
    self.selectionClickHandler = function(sourceObj) {

        if (!$(sourceObj).hasClass('grey-out')) {

            var eventId = $(sourceObj).data("eventid");
            var marketId = $(sourceObj).data("marketid");
            if (!$('#event_' + eventId).hasClass('grey-out')) {
                var selectionIdToAdd = $(sourceObj).parent().attr("id");
                if (selectionIdToAdd && selectionIdToAdd !== null) {
                    selectionIdToAdd = selectionIdToAdd.substring(selectionIdToAdd.indexOf("_") + 1);
                }
                if (selectionIdToAdd && selectionIdToAdd !== null && $(sourceObj).hasClass('active')) {
                    $(sourceObj).removeClass('active');
                    self.selectionArr.splice($.inArray(parseInt(selectionIdToAdd), self.selectionArr), 1);
                } else if (selectionIdToAdd && selectionIdToAdd !== null) {
                    $(sourceObj).addClass('active');
                    self.selectionArr.push(parseInt(selectionIdToAdd));
                }
                $(".bet-count").html(self.selectionArr.length);
            }
        }

    };

    /*
     * This method is responsible to take the user to RBS with the selections that were selected.
     */

    self.betNowClickHandler = function() {

        if (self.selectionArr && self.selectionArr.length > 0) {
            //alert("\n Redirecting to RBS with selectionids "+self.selectionArr);
            var rbs_url = configObj.RBS_URL + "&selections=" + self.selectionArr;
            self.logMessage("\n rbs_url " + self.selectionArr);
            OpenInNewTab(rbs_url);

        } else {
            alert("\n Please make atleast one selection");
        }

    };

    /** This method helps to open the link in new tab. */
    self.OpenInNewTab = function(url) {
        var win = window.open(url, '_blank');
        win.focus();
    };


    /*
     * This method is responsible to clear the selections that were selected.
     */
    self.clearBetsClickHandler = function() {
        if (self.selectionArr && self.selectionArr.length > 0) {
            for (var i in self.selectionArr) {
                var remSelectionId = self.selectionArr[i];
                $('#selection_' + remSelectionId).find('div').removeClass('active');
            }
        }
        self.selectionArr = [];
        $(".bet-count").html(self.selectionArr.length);
    };

    /*
     * listens to the selection Status updates and removes selections from bet now if any are present.
     * Hence user won't be able to select the selctions which was suspended/not displayed.
     */

    $(document).on("onRemoveSelection", function(selectionData) {
        if (self.selectionArr && self.selectionArr.length > 0 && $.inArray(parseInt(selectionData.selectionKey), self.selectionArr) > -1) {
            self.selectionArr.splice($.inArray(parseInt(selectionData.selectionKey), self.selectionArr), 1);
            $('#selection_' + selectionData.selectionKey).find('div').removeClass('active');
            $(".bet-count").html(self.selectionArr.length);
        }
    });

    /*
     * listens to the event Status updates and removes selections from bet now if any are present.
     * Hence user won't be able to select the selctions from the event which was suspended/not displayed.
     */

    $(document).on("onRemoveEvent", function(eventData) {
        if (self.selectionArr && self.selectionArr.length > 0) {
            $('#event_' + eventData.eventKey).find("*").each(function() {
                if ($(this).is('[id]')) {
                    self.logMessage(this); // "this" is the current element in the loop
                    var selectionId = $(this).attr("id");
                    var removeSelection = selectionId.substring(selectionId.indexOf("_") + 1);
                    if (removeSelection.indexOf('SUSP') == -1 && $.inArray(parseInt(removeSelection), self.selectionArr) > -1) {
                        $('#selection_' + removeSelection).find('div').removeClass('active');
                        self.selectionArr.splice($.inArray(parseInt(removeSelection), self.selectionArr), 1);
                        $(".bet-count").html(self.selectionArr.length);
                    }
                }
            });
        }

    });


    /*
     * listens to the Market Status updates and removes selections from bet now if any are present.
     * Hence user won't be able to select the selctions from the market which was suspended/not displayed.
     */
    $(document).on("onRemoveMarket", function(marketData) {
        if (self.selectionArr && self.selectionArr.length > 0) {
            $('#market_' + marketData.marketKey).next().find("*").each(function() {
                if ($(this).is('[id]')) {
                    self.logMessage(this); // "this" is the current element in the loop
                    var selectionId = $(this).attr("id");
                    self.logMessage("\n selectionID " + selectionId);
                    var removeSelection = selectionId.substring(selectionId.indexOf("_") + 1);
                    if (removeSelection.indexOf('SUSP') == -1 && $.inArray(parseInt(removeSelection), self.selectionArr) > -1) {
                        $('#selection_' + removeSelection).find('div').removeClass('active');
                        self.selectionArr.splice($.inArray(parseInt(removeSelection), self.selectionArr), 1);
                        $(".bet-count").html(self.selectionArr.length);
                    }
                }
            });
        }
    });

    return this;

})();

/******** My Bets functionality ********/
function getBetHistory(dataUrl) {
    $.ajax({
        type: 'get',
        url: dataUrl,
        success: function(json) {
            myBets.renderBetHistory(json);
        },
        error: function(err) {
            console.log("Error retrieving Bet History at " + dataUrl + ": " + err);
        }
    });
}

var myBets = (function() {

    this.htmlStr = "";

    this.renderBetHistory = function(json) {

        if (!jQuery.isEmptyObject(json.bets)) {

            // bets > bet[] > leg* > part > {bet details}
            // *could be a array eg. if accumulator
            var betsObj = json.bets.bet;

            this.htmlStr = "<table class=\"tblGrid\"><tbody>";
            for (var i = 0, j = betsObj.length; i < j; i++) {

                var leg = betsObj[i].leg;

                if (Array.isArray(leg)) {

                    var events = "",
                        markets = "",
                        selections = "",
                        odds = "";

                    for (var x = 0, y = leg.length; x < y; x++) {
                        var part = leg[x].part;
                        events += "<div>" + part.eventName + "</div>";
                        markets += "<div>" + part.marketName + "</div>"
                        selections += "<div>" + part.selectionName + "</div>"
                        odds += "<div>" + part.selectionPrice.decimalOdds + "</div>"
                    }
                    this.buildRowHtml(events, markets, selections, odds);

                } else {
                    var part = leg.part;
                    this.buildRowHtml(part.eventName, part.marketName,
                        part.selectionName, part.selectionPrice.decimalOdds);
                }
            }
            this.htmlStr += "</tbody></table>";
            $("#myBetsHistory .content .content").append(this.htmlStr);
        }
    };

    this.buildRowHtml = function(events, markets, selections, odds) {
        this.htmlStr += "<tr><td class=\"td-w40p\">" + events + "</td>" +
            "<td class=\"td-w10p\">" + markets + "</td>" +
            "<td class=\"td-w40p\">" + selections + "</td>" +
            "<td class=\"td-w10p\">" + odds + "</td></tr>";
    };

    return this;
})();