/**
 *  @fileOverview LadbrokesUIUpdater contains callback functions that process 
 *  Event/Market/Selection Updates and updates the UI elements accordingly.
 */

/** LadbrokesUIUpdater namespace
 * @namespace LadbrokesUIUpdater
 * 
 */

var LadbrokesUIUpdater = function() {

    return {

        /**
         * handles the event notification updates that are received and updates the UI event elements.
         * @param {object} serverEventObjAsArr contains the delta event update that is received.
         */

        eventUpdateNotificationHandler: function(serverEventObj) {

            if (serverEventObj.root['ns0:event'] && serverEventObj.root['ns0:event'].operation === "update") {

                var eventKey = serverEventObj.root['ns0:event'].eventKey;

                if (!$('#event_' + eventKey)) {
                    //console.log("eventKey not found ="+eventKey);
                    return;
                }

                var eventDivToUpdate = $('#event_' + eventKey).prev().find('div');
                var eventRowDiv = $('#event_' + eventKey);

                if (serverEventObj.root['ns0:event'].eventStatus) {
                    var eventStatus = serverEventObj.root['ns0:event'].eventStatus;

                    eventDivToUpdate.html("Event Status has changed to " + eventStatus).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);

                    if (eventStatus === "Suspended") {
                        $.event.trigger({ type: "onRemoveEvent", 'eventKey': eventKey });
                        eventRowDiv.addClass("grey-out");
                    } else if (eventStatus === "Active") {
                        eventRowDiv.removeClass("grey-out");
                    }

                } else if (serverEventObj.root['ns0:event'].displayStatus) {
                    var displayStatus = serverEventObj.root['ns0:event'].displayStatus;

                    eventDivToUpdate.html("Event Display Status has changed to " + displayStatus).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);

                    if (displayStatus === "NotDisplayed") {
                        $.event.trigger({ type: "onRemoveEvent", 'eventKey': eventKey });
                        eventRowDiv.addClass("grey-out");
                    } else if (displayStatus === "Displayed") {
                        eventRowDiv.removeClass("grey-out");
                    }

                } else if (serverEventObj.root['ns0:event'].displayOrder) {
                    var displayOrder = serverEventObj.root['ns0:event'].displayOrder;

                    eventDivToUpdate.html("Event Display Order changed to " + displayOrder).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);

                }

            }

        },

        /**
         * handles the market notification updates that are received and updates the UI market elements.
         * @param {object} serverMarketObjAsArr contains the delta market update that is received.
         */

        marketUpdateNotificationHandler: function(serverMarketObj) {

            if (serverMarketObj.root['ns0:market'] && serverMarketObj.root['ns0:market'].operation === "update") {

                var marketKey = serverMarketObj.root['ns0:market'].marketKey;
                if (!$('#market_' + marketKey)) {
                    //console.log("market not found ="+marketKey);
                    return;
                }

                var marketDivToUpdate = $('#market_' + marketKey).find('div');
                var marketRowDiv = $('#market_' + marketKey).next();

                if (serverMarketObj.root['ns0:market'].marketStatus) {

                    var marketStatus = serverMarketObj.root['ns0:market'].marketStatus;

                    marketDivToUpdate.html("Market Status has changed to " + marketStatus).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);
                    if (marketStatus === "Suspended") {
                        $.event.trigger({ type: "onRemoveMarket", 'marketKey': marketKey });
                        marketRowDiv.addClass("grey-out");
                    } else if (marketStatus === "Active") {
                        marketRowDiv.removeClass("grey-out");
                    }


                } else if (serverMarketObj.root['ns0:market'].displayStatus) {

                    var displayStatus = serverMarketObj.root['ns0:market'].displayStatus;

                    marketDivToUpdate.html("Market Display Status has changed to " + displayStatus).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);
                    if (displayStatus === "NotDisplayed") {
                        $.event.trigger({ type: "onRemoveMarket", 'marketKey': marketKey });
                        marketRowDiv.addClass("grey-out");
                    } else if (displayStatus === "Displayed") {
                        marketRowDiv.removeClass("grey-out");
                    }


                } else if (serverMarketObj.root['ns0:market'].displayOrder) {

                    var displayOrder = serverMarketObj.root['ns0:market'].displayOrder;

                    marketDivToUpdate.html("Market Display Order changed to " + displayOrder).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);

                }


            }

        },

        /**
         * handles the selection notification updates that are received and updates the UI selection elements.
         * @param {object} serverSelectionObjAsArr contains the delta selection update that is received.
         */

        selectionUpdateNotificationHandler: function(serverSelectionObj) {

            if (serverSelectionObj.root['ns0:selection'] && serverSelectionObj.root['ns0:selection'].operation === "update") {
                var selectionKey = serverSelectionObj.root['ns0:selection'].selectionKey;
                if (!$('#selection_' + selectionKey).length) {

                    //console.log("selectionkey not found ="+selectionKey);
                    return;
                }
                //This bit is to update the dive-note-disp
                var selectionDiv = $('#' + selectionKey); //div with id="611425691"
                var selectionDivToUpdate = null;

                if ($('#selection_' + selectionKey).parent().is('td')) {
                    selectionDivToUpdate = $('#selection_' + selectionKey).parent().parent().prev().find('div');
                } else if ($('#selection_' + selectionKey).parent().is('tr')) {
                    selectionDivToUpdate = $('#selection_' + selectionKey).parent().prev().find('div');
                }
                //disp 
                if (serverSelectionObj.root['ns0:selection'].currentPrice) {
                    var oldPrice = selectionDiv.html().trim();
                    var updatedNumPrice = serverSelectionObj.root['ns0:selection'].currentPrice.numPrice;
                    var updatedDenPrice = serverSelectionObj.root['ns0:selection'].currentPrice.denPrice;
                    var updatedDecPrice = serverSelectionObj.root['ns0:selection'].currentPrice.decimalPrice;


                    var updatedPrice = updatedNumPrice + "/" + updatedDenPrice;
                    //console.log("\n updatedPrice "+updatedPrice);
                    selectionDiv.html(updatedPrice.trim());

                    $('#' + selectionKey).addClass("blink_me").delay(5000).queue(function() {
                        $(this).removeClass("blink_me");
                        $(this).dequeue();
                    });
                    //var comparisonValue = compareOdds(updatedPrice.trim(),oldPrice);
                    //console.log("\n comparisonValue==="+comparisonValue);				

                } else if (serverSelectionObj.root['ns0:selection'].selectionStatus) {

                    var selectionStatus = serverSelectionObj.root['ns0:selection'].selectionStatus;
                    selectionDivToUpdate.html("Selection Status has changed to " + selectionStatus).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);
                    if (selectionStatus === "Suspended") {
                        $.event.trigger({ type: "onRemoveSelection", 'selectionKey': selectionKey });
                        selectionDiv.addClass("grey-out").html('SUSP');
                    } else if (selectionStatus === "Active") {
                        if (selectionDiv.hasClass("grey-out")) {
                            selectionDiv.removeClass("grey-out");
                        } else {

                        }
                    }

                } else if (serverSelectionObj.root['ns0:selection'].displayStatus) {

                    var displayStatus = serverSelectionObj.root['ns0:selection'].displayStatus;
                    selectionDivToUpdate.html("Selection Display Status has changed to " + displayStatus).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);

                    if (displayStatus === "NotDisplayed") {
                        $.event.trigger({ type: "onRemoveSelection", 'selectionKey': selectionKey });
                        selectionDiv.addClass("grey-out");
                    } else if (displayStatus === "Displayed") {
                        if (selectionDiv.hasClass("grey-out")) {
                            selectionDiv.removeClass("grey-out");
                        }
                    }

                } else if (serverSelectionObj.root['ns0:selection'].displayOrder) {

                    var displayOrder = serverSelectionObj.root['ns0:selection'].displayOrder;
                    selectionDivToUpdate.html("Selection Display Order changed to " + displayOrder).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);
                    revSelectionObj.setSelectionDisplayOrder(displayOrder);

                }

            }
        },
        // // // for the favourites clones

        // /**
        //  * handles the event notification updates that are received and updates the UI event elements.
        //  * @param {object} serverEventObjAsArr contains the delta event update that is received.
        //  */

        // eventUpdateNotificationHandlerClone: function(serverEventObj) {

        //     if (serverEventObj.root['ns0:event'] && serverEventObj.root['ns0:event'].operation === "update") {

        //         var eventKey = serverEventObj.root['ns0:event'].eventKey + 'fav';

        //         if (!$('#event_' + eventKey)) {
        //             //console.log("eventKey not found ="+eventKey);
        //             return;
        //         }

        //         var eventDivToUpdate = $('#event_' + eventKey).prev().find('div');
        //         var eventRowDiv = $('#event_' + eventKey);

        //         if (serverEventObj.root['ns0:event'].eventStatus) {
        //             var eventStatus = serverEventObj.root['ns0:event'].eventStatus;

        //             eventDivToUpdate.html("Event Status has changed to " + eventStatus).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);

        //             if (eventStatus === "Suspended") {
        //                 $.event.trigger({ type: "onRemoveEvent", 'eventKey': eventKey });
        //                 eventRowDiv.addClass("grey-out");
        //             } else if (eventStatus === "Active") {
        //                 eventRowDiv.removeClass("grey-out");
        //             }

        //         } else if (serverEventObj.root['ns0:event'].displayStatus) {
        //             var displayStatus = serverEventObj.root['ns0:event'].displayStatus;

        //             eventDivToUpdate.html("Event Display Status has changed to " + displayStatus).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);

        //             if (displayStatus === "NotDisplayed") {
        //                 $.event.trigger({ type: "onRemoveEvent", 'eventKey': eventKey });
        //                 eventRowDiv.addClass("grey-out");
        //             } else if (displayStatus === "Displayed") {
        //                 eventRowDiv.removeClass("grey-out");
        //             }

        //         } else if (serverEventObj.root['ns0:event'].displayOrder) {
        //             var displayOrder = serverEventObj.root['ns0:event'].displayOrder;

        //             eventDivToUpdate.html("Event Display Order changed to " + displayOrder).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);

        //         }

        //     }

        // },


        // /**
        //  * handles the selection notification updates that are received and updates the UI selection elements.
        //  * @param {object} serverSelectionObjAsArr contains the delta selection update that is received.
        //  */

        // selectionUpdateNotificationHandlerClone: function(serverSelectionObj) {

        //     if (serverSelectionObj.root['ns0:selection'] && serverSelectionObj.root['ns0:selection'].operation === "update") {
        //         var selectionKey = serverSelectionObj.root['ns0:selection'].selectionKey + 'fav';
        //         if (!$('#selection_' + selectionKey).length) {

        //             //console.log("selectionkey not found ="+selectionKey);
        //             return;
        //         }
        //         //This bit is to update the dive-note-disp
        //         var selectionDiv = $('#' + selectionKey);
        //         var selectionDivToUpdate = null;

        //         if ($('#selection_' + selectionKey).parent().is('td')) {
        //             selectionDivToUpdate = $('#selection_' + selectionKey).parent().parent().prev().find('div');
        //         } else if ($('#selection_' + selectionKey).parent().is('tr')) {
        //             selectionDivToUpdate = $('#selection_' + selectionKey).parent().prev().find('div');
        //         }
        //         //disp 
        //         if (serverSelectionObj.root['ns0:selection'].currentPrice) {
        //             var oldPrice = selectionDiv.html().trim();
        //             var updatedNumPrice = serverSelectionObj.root['ns0:selection'].currentPrice.numPrice;
        //             var updatedDenPrice = serverSelectionObj.root['ns0:selection'].currentPrice.denPrice;
        //             var updatedDecPrice = serverSelectionObj.root['ns0:selection'].currentPrice.decimalPrice;


        //             var updatedPrice = updatedNumPrice + "/" + updatedDenPrice;
        //             //console.log("\n updatedPrice "+updatedPrice);
        //             selectionDiv.html(updatedPrice.trim());

        //             $('#' + selectionKey + 'fav').addClass("blink_me").delay(5000).queue(function() {
        //                 $(this).removeClass("blink_me");
        //                 $(this).dequeue();
        //             });
        //             //var comparisonValue = compareOdds(updatedPrice.trim(),oldPrice);
        //             //console.log("\n comparisonValue==="+comparisonValue);				

        //         } else if (serverSelectionObj.root['ns0:selection'].selectionStatus) {

        //             var selectionStatus = serverSelectionObj.root['ns0:selection'].selectionStatus;
        //             selectionDivToUpdate.html("Selection Status has changed to " + selectionStatus).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);
        //             if (selectionStatus === "Suspended") {
        //                 $.event.trigger({ type: "onRemoveSelection", 'selectionKey': selectionKey });
        //                 selectionDiv.addClass("grey-out").html('SUSP');
        //             } else if (selectionStatus === "Active") {
        //                 if (selectionDiv.hasClass("grey-out")) {
        //                     selectionDiv.removeClass("grey-out");
        //                 } else {

        //                 }
        //             }

        //         } else if (serverSelectionObj.root['ns0:selection'].displayStatus) {

        //             var displayStatus = serverSelectionObj.root['ns0:selection'].displayStatus;
        //             selectionDivToUpdate.html("Selection Display Status has changed to " + displayStatus).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);

        //             if (displayStatus === "NotDisplayed") {
        //                 $.event.trigger({ type: "onRemoveSelection", 'selectionKey': selectionKey });
        //                 selectionDiv.addClass("grey-out");
        //             } else if (displayStatus === "Displayed") {
        //                 if (selectionDiv.hasClass("grey-out")) {
        //                     selectionDiv.removeClass("grey-out");
        //                 }
        //             }

        //         } else if (serverSelectionObj.root['ns0:selection'].displayOrder) {

        //             var displayOrder = serverSelectionObj.root['ns0:selection'].displayOrder;
        //             selectionDivToUpdate.html("Selection Display Order changed to " + displayOrder).show().effect("highlight", { color: '#504f4f', mode: 'hide' }, 10000);
        //             revSelectionObj.setSelectionDisplayOrder(displayOrder);

        //         }

        //     }
        // }

    };
}();