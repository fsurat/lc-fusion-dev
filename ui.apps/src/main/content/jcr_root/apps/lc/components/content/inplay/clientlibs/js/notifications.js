/**
*  @fileOverview LadbrokesNotifications contains functions that handles Event/Market/Selection subscriptions and 
*  notifies to the notification Handlers.
*/

/** LadbrokesNotifications namespace
* @namespace LadbrokesNotifications
* 
*/

var LadbrokesNotifications = function(){
	
	/**
	 * private function used to create the destination.
	 * @param {string} name topic name
	 * @param {string} session session Object
	 */
	function createDestination(name, session) 
	{
		if (name.indexOf("/topic/") == 0) {
		    return session.createTopic(name);
		}else {
		    throw new Error("Destination must start with /topic/");
		}
	}
	
	
	/** classMessageProcessor which process the updates initially.
	 * @param {string} message update that is received. 
	 * 
	 */
	var classMessageProcessor = function(message){
		
		if (message instanceof TextMessage) {

			var body = message.getText();
			
			var jsonObj = parseJSONResponse(body);
			
			if(jsonObj.root['ns0:selection'] && jsonObj.root['ns0:selection'].operation === "update"){
				
				var selectionKey = jsonObj.root['ns0:selection'].selectionKey;
				LadbrokesUIUpdater.selectionUpdateNotificationHandler(jsonObj);
				
			}else if(jsonObj.root['ns0:market'] && jsonObj.root['ns0:market'].operation === "update"){
				
				var marketKey = jsonObj.root['ns0:market'].marketKey;			
				LadbrokesUIUpdater.marketUpdateNotificationHandler(jsonObj);
					
			}else if(jsonObj.root['ns0:event'] && jsonObj.root['ns0:event'].operation === "update"){
				
				var eventKey = jsonObj.root['ns0:event'].eventKey;			
				LadbrokesUIUpdater.eventUpdateNotificationHandler(jsonObj);
					
			}		
		}
		
	};
	
	return {
		
		/**
		 * subscribes to the static topic and class selector.
		 * 
		 * @param {object} connectionWrapperObj connection wrapper object which encapsulates connection and session objects.
		 * @param {string} topicName topicName to subscribe for listening the updates.
		 * @param {string} messageSelector messageSelector to filter required live feeds.
		 */
		
		doClassSubscription : function(connectionWrapperObj, topicName, messageSelector){
			
			console.log("\n subscribing to "+topicName);
			var session = connectionWrapperObj["session"];		
			var dest = createDestination(topicName, session);	
			var consumer = "";
		    if (messageSelector && messageSelector.length > 0) {
		        consumer = session.createConsumer(dest, messageSelector);
		    } else {
		        consumer = session.createConsumer(dest);
		        //console.log("SUBSCRIBE: " + name + " subscriptionTag"+destinationId);	
		    }	
			consumer.setMessageListener(classMessageProcessor);			
		},
	};
	
}();