/**
 * This is responsible to handle the Tab event notifications, as well as slide actions.
 */

$(function() {
    $("#tab-wrapper li").mouseover(function() {
        $(this).prev().css("background-color", "#d3291f");
    }).mouseout(function() {
        $(this).prev().css("background-color", "#767676");

    });

    $("#tab-wrapper li.active").addClass(function() {
        $(this).prev().css("background-color", "#d3291f");

    }).mouseout(function() {
        $(this).prev().css("background-color", "#d3291f");

    });

});

$(document).ready(function() {

    $(".heading").on("click", function() {

        $(this).next(".content").slideToggle(500);

        if ($(this).children('h2')) {

            if ($(this).children('h2').hasClass("downarrow-img")) {
                $(this).children('h2').removeClass("downarrow-img").addClass("rtarrow-img");
            } else if ($(this).children('h2').hasClass("rtarrow-img")) {
                $(this).children('h2').removeClass("rtarrow-img").addClass("downarrow-img");
            }

        }

    });

    $("#allTab").on("click", function() {
        $('#footBallContent').show();
        $('#tennisContent').show();
        $('#myBetsContent').hide();
        $('#myBetsFavourites').hide();
        $('#allTab').addClass('active');
        $('#footBallTab').removeClass('active');
        $('#tennisTab').removeClass('active');
        $('#myBetsTab').removeClass('active');
    });

    $("#footBallTab").on("click", function() {
        $('#footBallContent').show();
        $('#tennisContent').hide();
        $('#myBetsContent').hide();
        $('#myBetsFavourites').hide();
        $('#allTab').removeClass('active');
        $('#footBallTab').addClass('active');
        $('#tennisTab').removeClass('active');
        $('#myBetsTab').removeClass('active');
    });

    $("#tennisTab").on("click", function() {
        $('#footBallContent').hide();
        $('#tennisContent').show();
        $('#myBetsContent').hide();
        $('#myBetsFavourites').hide();
        $('#allTab').removeClass('active');
        $('#footBallTab').removeClass('active');
        $('#tennisTab').addClass('active');
        $('#myBetsTab').removeClass('active');
    });

    $("#myBetsTab").on("click", function() {
        $('#footBallContent').hide();
        $('#tennisContent').hide();
        $('#myBetsContent').show();
        $('#myBetsFavourites').show();
        $('#allTab').removeClass('active');
        $('#footBallTab').removeClass('active');
        $('#tennisTab').removeClass('active');
        $('#myBetsTab').addClass('active');
    });

    $('.btn_lg').on("click", bip_main.betNowClickHandler);

    $('.btn_lg-cb').on("click", bip_main.clearBetsClickHandler);
});


/**
 * This function is helpful to notify the user hide or show icons.
 * @param selectedTitleDiv : div to hide or show.
 */
function toogleClick(selectedTitleDiv) {
    $(selectedTitleDiv).next(".content").slideToggle(500);
    if ($(selectedTitleDiv).children('span')) {
        if ($(selectedTitleDiv).children('span').hasClass("minus-img")) {
            $(selectedTitleDiv).children('span').removeClass("minus-img").addClass("plus-img");
        } else if ($(selectedTitleDiv).children('span').hasClass("plus-img")) {
            $(selectedTitleDiv).children('span').removeClass("plus-img").addClass("minus-img");
        }
    }
}