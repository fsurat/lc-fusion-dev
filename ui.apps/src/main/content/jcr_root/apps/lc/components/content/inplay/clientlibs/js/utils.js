/*
 * Helper js which has helper functions.
 */
function getCurrentUserTime(k) {
	if(k){
	   console.log("\n getCurrentUserTime "+k);
       var d = new Date(k.replace(/\-/g,"/").replace(/T/," ").replace(/.001Z/," -0"));
       inputTime = d.getTime();
       inputTimeOffset = d.getTimezoneOffset() * 60000;
       var i = inputTime + inputTimeOffset;
       var l = new Date();
       var h = (l.getTimezoneOffset() / 60);
       var j = i - (3600000 * h);
       localDate = new Date(j);
       var getHours = localDate.getHours();
       var getMinutes = localDate.getMinutes();
       //localDate.getMinutes = 9;
       if(getHours < 10) 
              getHours = '0'+getHours;
       if(getMinutes < 10)
              getMinutes = '0'+getMinutes;
       var yearandtime = localDate.getDate()+"/"+((localDate.getMonth())+1)+"/"+localDate.getFullYear()+"-"+getHours+":"+getMinutes;
       
       var getTimeOnly = getHours+":"+getMinutes;
       return getTimeOnly;
	}else{
	   return ""
	}
		
};

var convertOddsToNumber = function(odds) {
    if(odds === 'SP') {
	    return odds;
    }

    var oddsArray = odds.split('/');
    odds = parseInt(oddsArray[0], 10)/parseInt(oddsArray[1], 10);
    return odds;
};

var compareOdds = function(odds_a, odds_b){
    odds_a = convertOddsToNumber(odds_a);
    odds_b = convertOddsToNumber(odds_b);
    if( odds_a > odds_b )
	    return 1;
    else
	    return -1;
};

function parseJSONResponse(a) {
	var b = null;
	try
	{
	if (a.constructor === String) {
		b = jQuery.parseJSON(a);
	} else {
		b = a;
	}
	}
	catch(d)
	{
		//alert("parseJSONResponse"+d)
	}
	return b;
}