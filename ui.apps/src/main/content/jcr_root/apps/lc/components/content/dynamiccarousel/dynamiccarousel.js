'use strict';

use(function() {

	var currNode = com.day.cq.wcm.commons.WCMUtils.getNode(resource);
	var nodeStatus = currNode !=null ? currNode.hasNode("carouselItems") : false;
	var carouselList = [];
	
	if (nodeStatus) {
	
		var listItemsNode = currNode.getNode("carouselItems");
		var listItemsNodes = listItemsNode.getNodes();

		for (var i = 0; i < listItemsNodes.length; i++) {
			var data = {
				"bannerURL" : listItemsNodes[i].imageUrl,
				"bannerLink" : listItemsNodes[i].linkUrl,
				"topicId" : listItemsNodes[i].topicId
			}
			carouselList.push(data);
		}
	}

	return {
	  carouselList : carouselList
	};

});