/**
 *  @fileOverview LadbrokesWebFactory provides an interface for creating new WebSocket connection to the Ladbrokes Server
 * 	for receiving real-time updates of in-play Event/Market/Selection Updates.
*/

/** LadbrokesWebFactory namespace
 * @namespace LadbrokesWebFactory
 * 
 */
var LadbrokesWebFactory = function(){
	var conWrapperObj = {};
	var connection = null;	
	var connectionFuture = null;
	var connectionCallback = null;
	var maxRetries = 5;
	var retry = 0;
	
	 /**
     * private callback function, will be invoked when the connection is available after successful authentication.
     *  
     */
	
	function onConnect(){			 
        try {        	
            connection = connectionFuture.getValue();
            connection.setExceptionListener = function(e) {
                alert("Connection Error: "+e.getMessage());
            };            
            conWrapperObj["connection"] = connection; 
           	var session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
           	conWrapperObj["session"] = session;
           	connectionCallback(conWrapperObj);
           	connection.start(function () { /* Started */ 
        		console.log("\n In Connection Started ");	
        		retry--;
           	});
        }catch (e) {
                alert(e.message);
        }
	}
	
	/**
	 * private function, Responsible for managing a concept of a default challenge handler.
	 * 
	 */
	
	function ChallengeHandler(apiKey){
		
		this.canHandle = function(challengeRequest) {
		    console.log("challengeRequest="+JSON.stringify(challengeRequest));
			return challengeRequest != null && "Basic" == challengeRequest.authenticationScheme;
		};
		
		this.handle = function(challengeRequest, callback) {
			console.log("in challenge Handle");
		    var challengeResponse = null;
		    if (retry++ >= maxRetries) {
		        callback(null);       // abort authentication process if reaches max retries
		     }else{
			    if (challengeRequest.location !== null) {		    		
			            //set the token to challengeResponse
			            var base64EncodedApiKey = $.base64('encode', apiKey);
			            //console.log("\n base64EncodedApiKey="+base64EncodedApiKey);
			            challengeResponse = new ChallengeResponse("Basic "+base64EncodedApiKey, this);
			    }
		     }
		    //invoke callback function with challenge response
		    callback(challengeResponse);
		};
	}
	
	/**
	 * private function, used to initialize the challenge handlers
	 * whenever a request that matches one of the specific locations encounters a 401 challenge from the server, 
	 * the corresponding ChallengeHandler object is invoked to handle the challenge 
	 */
	
	function initializeChallengeHandlers(webSocketFactory,twmUrl,apiKey){
		var challengeHandler = new ChallengeHandler(apiKey);
		var dispatchHandler = new DispatchChallengeHandler();
		dispatchHandler.register(twmUrl, challengeHandler);		
		webSocketFactory.setChallengeHandler(dispatchHandler);
	}	
	
	return {
		/** createConnection is to create a websocket connection to the target url specified. 
		 * A WebSocket is used to establish the connection. 
		 * 
		 * @param {string} url The url should specify a protocol such as ws: or wss:. For example: ws://localhost:8000/jms
 		 * @param {string} apikey key that is used to authenticate your application
		 * @param {string} username The username for authentication or null (optional)
		 * @param {string} password The password for authentication or null (optional)
		 * @param {function} fn The callback to invoke when the connection has been created. 
		 */
		createConnection : function(url,apikey,username,password,fn){
			connectionCallback = fn;
			var connectionFactory = new JmsConnectionFactory(url);		
			if (connection === null)
			{
				if(typeof Tracer !== 'undefined'){
					//Tracer.setTrace(true);
				}
				
				initializeChallengeHandlers(connectionFactory.getWebSocketFactory(), url, apikey);
				connectionFuture = connectionFactory.createConnection(username, password, onConnect);
			 }
		},
		
		/** closeConnection is to close the websocket connection which was established. 
		 */
		closeConnection: function(){
        	if(connection != null){	
		        try {
		            connection.close(function () { /* Closed */
		            	console.log("\n Connection Closed ");
		            });
		        }
		        finally {
		            connection = null;
		        }
	            
			}			
		}
	};
}();	