// initiate the swiper plugin for carousel
$(document).ready(function() {
    var mySwiper = new Swiper('.dynamic-carousel', {
        // Optional parameters
        loop: true,
        speed: 1000,
        autoplay: 4000,
        touchEventsTarget: 'carousel__slide',
        autoHeight: true,
        // If we need pagination
        pagination: '.swiper-pagination',

        // Navigation arrows
        nextButton: '.swiper-button-next-2',
        prevButton: '.swiper-button-prev-2'

    })

    // connect to diffusion server
    // diffusion.connect({
    //     //host : 'ec2-35-177-255-83.eu-west-2.compute.amazonaws.com',
    //     host: 'localhost',
    //     port: '8080',
    //     // To connect anonymously you can leave out the following parameters
    //     principal: 'client',
    //     credentials: 'password'
    // }).then(subscribeToTopic);

});

// subscribes to topics defined in the HTML and updates the image upon new data
function subscribeToTopic(session) {
    var topics = document.getElementsByClassName("topic");
    for (var i = 0; i < topics.length; i++) {

        var topicId = topics.item(i).getAttribute("data-topic-id");

        session.subscribe(topicId);
        session.stream(topicId).asType(diffusion.datatypes.json())
            .on('value', function(topic, specification, newValue, oldValue) {

                console.log("Update for " + topic, newValue.get());
                var jsonObj = newValue.get();

                // change this depending on the JSON structure from data feed
                var odds = jsonObj.event123.odds;

                // duplicate images are generated per slide within the HTML.
                // all images associated with a specific slide have the same class (topic id).
                var banners = document.getElementsByClassName(topic);
                for (var j = 0; j < banners.length; j++) {

                    // images should be Scene7 images which takes a variables as querystring params
                    // eg. http://s7g10.scene7.com/is/image/DeloittePartnerSandbox/oddsBanner?$carouselpreset$&$imgsrc=is{DeloittePartnerSandbox%2FCricket}&$customerOfferType=NEW%20CUSTOMER%20OFFER&$font_12_Arial-Black=DefaultFont
                    var imgUrl = banners.item(j).getAttribute("src");
                    var urlObj = new URL(imgUrl);

                    var params = urlObj.searchParams;

                    if (params.toString().length > 0) {
                        if (params.has("$oddsValue")) {
                            params.set("$oddsValue", odds);
                        } else {
                            params.append("$oddsValue", odds);
                        }

                        console.log("URL: " + urlObj.toString());
                        banners.item(j).setAttribute("src", decodeURIComponent(urlObj.toString()));
                    }
                }
            });
    }
}