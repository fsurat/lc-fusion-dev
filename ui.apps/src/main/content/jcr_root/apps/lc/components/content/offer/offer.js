'use strict';

use(function() {

	var imageUrl = properties.get("imageUrl", "");
	var linkUrl  = properties.get("linkUrl",  "");
	var topicId  = properties.get("topicId",  "");

	var enableSchedule  = properties.get("enableSchedule",  false);
	var scheduleOn      = properties.get("scheduleOn",  "");
	var scheduleOff     = properties.get("scheduleOff", "");

	return {
		"imageUrl" : imageUrl,
		"linkUrl" : linkUrl,
		"topicId" : topicId,
		"enableSchedule" : enableSchedule,
		"scheduleOn" : scheduleOn,
		"scheduleOff" : scheduleOff
	}
});
