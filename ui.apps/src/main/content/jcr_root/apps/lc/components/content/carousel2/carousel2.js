'use strict';

use(function() {

    var currNode = com.day.cq.wcm.commons.WCMUtils.getNode(resource);
    var nodeStatus = currNode != null ? currNode.hasNode("carouselItems") : false;
    var carouselList = [];


    var UID = properties.get("uid", "1");
    var themeColor = properties.get("themeColor", "white");
    var transitionSpeed = properties.get("transitionSpeed", 1000);
    var slideDuration = properties.get("slideDuration", 4000);
    var paginationEnabled = properties.get("paginationEnabled");
    var arrowsEnabled = properties.get("arrowsEnabled");

    paginationEnabled = (paginationEnabled == "true") ? "show-pagination" : "";
    arrowsEnabled = (arrowsEnabled == "true") ? "show-arrows" : "";
    UID = 'carousel' + UID;
    var UIDclass = '".' + UID + '"';
    var UIDbuttonClassPrev = '".' + UID + '-prev"';
    var UIDbuttonClassNext = '".' + UID + '-next"';


    if (nodeStatus) {

        var listItemsNode = currNode.getNode("carouselItems");
        var listItemsNodes = listItemsNode.getNodes();
        for (var i = 0; i < listItemsNodes.length; i++) {
            var data = {
                "bannerURL": listItemsNodes[i].imageUrl,
                "bannerLink": listItemsNodes[i].linkUrl
            };
            carouselList.push(data);
        }

    }

    return {
        carouselList: carouselList,
        UID: UID,
        UIDclass: UIDclass,
        UIDbuttonClassPrev: UIDbuttonClassPrev,
        UIDbuttonClassNext: UIDbuttonClassNext,
        themeColor: themeColor,
        transitionSpeed: transitionSpeed,
        slideDuration: slideDuration,
        paginationEnabled: paginationEnabled,
        arrowsEnabled: arrowsEnabled

    };

});