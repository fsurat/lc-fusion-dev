var ladbrokes = {};
ladbrokes.Config = {   
	USERPREF : {
	     apikey : 'LAD5f9bd8306d6a442fa74790315650fc7a',
	     locale : 'en-GB',
	     filter : 'event.is-live-now-event',
	     expand : 'event',
	     webSocketUrl : 'wss://ws.ladbrokes.com/jms',
	     host : 'https://api.ladbrokes.com',
	     topic : '/topic/Ladbrokes.GBR.Service.Public.Notify.Technical.Kaazing.KaazingPublisher.1',
	     classKeys : ['110000006', '110000009'],
	     selector : "ClassKey='110000006' OR ClassKey='110000009'",
	     setSelector: function(userPrefs){
		    	var classKeysStr = '';
		    	var userPrefsArr = userPrefs.split(',');
		    	
		    	// build selector string based on user preferences
		    	for (var i = 0, len = userPrefsArr.length; i < len; i++) {
		    		
		    		userPrefsArr[i] = userPrefsArr[i].trim();
		    		
		    		if (i == 0) {
		    			classKeysStr += "ClassKey='" + userPrefsArr[i] + "'";
		    		} else {
		    			classKeysStr += " OR ClassKey='" + userPrefsArr[i] + "'";
		    		}
		    	}
		    	this.classKeys = userPrefsArr;
		    	this.selector = classKeysStr;
		     },
	     getClassCollectionUrlArray: function(){
			var classArr = [];
			
			for (var i = 0, len = this.classKeys.length; i < len; i++) {
				var classType = {'CLASSTYPE' : {
						'CLASS_URL' : this.host+'/v2/sportsbook-api/classes/' + this.classKeys[i] + '?locale='+this.locale+'&api-key='+this.apikey+'&expand=event'+'&filter='+this.filter+'&no-links=1',
						'EVENT_URL' : this.host+'/v2/sportsbook-api/classes/' + this.classKeys[i] + '/types/null/subtypes/null/events/EVENT_KEY?locale='+this.locale+'&api-key='+this.apikey+'&expand=selection&decorator=undisplayed'
					}
				}
				classArr.push(classType);
			}
			return classArr;
		 },
		 RBS_URL : "https://betslip.ladbrokes.com/RemoteBetslip/bets/betslip.html?locale=en-GB&aff-tag=653046&aff-id=653046"
   },
	LEEDSUAT : {
	     apikey : 'l7xxea0d4845e6d24201ad283e49a291231c',
	     locale : 'en-GB',
	     filter : 'event.is-live-now-event',
	     expand : 'event',	     
	     webSocketUrl : 'wss://uatws.ladbrokes.com/jms',
	     host : 'https://uat-api.ladbrokes.com',
	     topic : '/topic/Ladbrokes.GBR.Service.Private.Notify.Technical.Kaazing.KaazingPublisher.2',
	     selector : "ClassKey='110000006' OR ClassKey='110000009'",	     
	     getClassCollectionUrlArray: function(){
	    	 return [
		    	        {'CLASSTYPE' : {
		    	        			   'CLASS_URL' : this.host+'/v2/sportsbook-api/classes/110000006?locale='+this.locale+'&api-key='+this.apikey+'&expand=event'+'&filter='+this.filter+'&no-links=1',
		    		 				   'EVENT_URL' : this.host+'/v2/sportsbook-api/classes/110000006/types/subtypes/events/EVENT_KEY?locale='+this.locale+'&api-key='+this.apikey+'&expand=selection&decorator=undisplayed'
		    		 				  }
		    	        },
		    		 	{'CLASSTYPE' : {
		    		 					'CLASS_URL' : this.host+'/v2/sportsbook-api/classes/110000009?locale='+this.locale+'&api-key='+this.apikey+'&expand=event'+'&filter='+this.filter+'&no-links=1',
			    		 			    'EVENT_URL' : this.host+'/v2/sportsbook-api/classes/110000009/types/subtypes/events/EVENT_KEY?locale='+this.locale+'&api-key='+this.apikey+'&expand=selection&decorator=undisplayed'
			    		 			  }
		    	        }
	    	        ];
		  },
		  RBS_URL : "https://betslip.ladbrokes.com/RemoteBetslip/bets/betslip.html?locale=en-GB&aff-tag=653046&aff-id=653046"
   },   
   SWINPROD : {
	     apikey : 'LAD5f9bd8306d6a442fa74790315650fc7a',
	   	 //apikey : 'l7xx6fc86fa5b9cd47be98a80ae28cf27306',
	     locale : 'en-GB',
	     filter : 'event.is-live-now-event',
	     expand : 'event',	     
	     //filter :'has-next-24hr-event',
	     //webSocketUrl : 'ws://ldsrpapiwwwp001.ladsys.net:8001/jms',		     		     
	     //webSocketUrl : 'wss://ldsrpapiwwwp001.ladsys.net:443/jms',
	     webSocketUrl : 'wss://ws.ladbrokes.com/jms',
	     host : 'https://api.ladbrokes.com',
	     //topic : '/topic/Ladbrokes.GBR.Service.Private.Notify.Technical.Kaazing.KaazingPublisher.2',
	     topic : '/topic/Ladbrokes.GBR.Service.Public.Notify.Technical.Kaazing.KaazingPublisher.1',
	     selector : "ClassKey='110000006' OR ClassKey='110000009'",
	     getClassCollectionUrlArray: function(){
	    	 return [
		    	        {'CLASSTYPE' : {
		    	        			   'CLASS_URL' : this.host+'/v2/sportsbook-api/classes/110000006?locale='+this.locale+'&api-key='+this.apikey+'&expand=event'+'&filter='+this.filter+'&no-links=1',
		    		 				   'EVENT_URL' : this.host+'/v2/sportsbook-api/classes/110000006/types/null/subtypes/null/events/EVENT_KEY?locale='+this.locale+'&api-key='+this.apikey+'&expand=selection&decorator=undisplayed'
		    		 				  }
		    	        },
		    		 	{'CLASSTYPE' : {
		    		 					'CLASS_URL' : this.host+'/v2/sportsbook-api/classes/110000009?locale='+this.locale+'&api-key='+this.apikey+'&expand=event'+'&filter='+this.filter+'&no-links=1',
			    		 			    'EVENT_URL' : this.host+'/v2/sportsbook-api/classes/110000009/types/null/subtypes/null/events/EVENT_KEY?locale='+this.locale+'&api-key='+this.apikey+'&expand=selection&decorator=undisplayed'
			    		 			  }
		    	        }
	    	        ];
		  },
		  RBS_URL : "https://betslip.ladbrokes.com/RemoteBetslip/bets/betslip.html?locale=en-GB&aff-tag=653046&aff-id=653046"
		  
   },
   PERF : {
	     apikey : 'LAD5f9bd8306d6a442fa74790315650fc7a',
	     locale : 'en-GB',
	     filter : 'event.is-live-now-event',
	     expand : 'event',	     
	     webSocketUrl : 'wss://perf-ws.ladbrokes.com/jms',
	     host : 'https://perf-api.ladbrokes.com',
	     topic : '/topic/Ladbrokes.GBR.Service.Private.Notify.Technical.Kaazing.KaazingPublisher.2',	
	     selector : "ClassKey='110000006' OR ClassKey='110000009'",	     
	     getClassCollectionUrlArray: function(){
	    	 return [
		    	        {'CLASSTYPE' : {
		    	        			   'CLASS_URL' : this.host+'/v2/sportsbook-api/classes/110000006?locale='+this.locale+'&api-key='+this.apikey+'&expand=event'+'&filter='+this.filter+'&no-links=1',
		    		 				   'EVENT_URL' : this.host+'/v2/sportsbook-api/classes/110000006/types/subtypes/events/EVENT_KEY?locale='+this.locale+'&api-key='+this.apikey+'&expand=selection&decorator=undisplayed'
		    		 				  }
		    	        },
		    		 	{'CLASSTYPE' : {
		    		 					'CLASS_URL' : this.host+'/v2/sportsbook-api/classes/110000009?locale='+this.locale+'&api-key='+this.apikey+'&expand=event'+'&filter='+this.filter+'&no-links=1',
			    		 			    'EVENT_URL' : this.host+'/v2/sportsbook-api/classes/110000009/types/subtypes/events/EVENT_KEY?locale='+this.locale+'&api-key='+this.apikey+'&expand=selection&decorator=undisplayed'
			    		 			  }
		    	        }
	    	        ];
		  },
		  RBS_URL : "https://betslip.ladbrokes.com/RemoteBetslip/bets/betslip.html?locale=en-GB&aff-tag=653046&aff-id=653046"		  
 }  
};