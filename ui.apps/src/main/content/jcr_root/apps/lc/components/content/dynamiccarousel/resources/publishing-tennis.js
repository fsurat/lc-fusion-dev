/*******************************************************************************
 * Copyright (C) 2014, 2017 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

const diffusion = require('diffusion');

diffusion.connect({
	host : 'localhost',
	port : '8080',
	principal : 'control',
	credentials : 'password'
}).then(function(session) {

	var topicId = "tennis/wimbledon";
	var i = 5;

	console.log('Connected! Topic ID: ' + topicId);

	// Create a JSON topic
	session.topics.add(topicId, diffusion.topics.TopicType.JSON);

	// Start updating the topic at regular intervals
	setInterval(function() {
		var jsonObj = {
			"event123": {
				"odds": i.toFixed(2)
			}
		};

		session.topics.update(topicId, jsonObj);
		i += 0.01;
		console.log(JSON.stringify(jsonObj));
	}, 5000);
});